<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Live Webcast</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
</head>

<body id="bg">
<!-- <div class="top">
        <img src="img/top-banner.jpg" width="100%" alt=""/> 
</div> -->
<nav class="navbar m-0 p-0">
        <img src="img/kingfisherlogo.png" width="150px" class="text-center" alt="">
         <img src="img/kkrlogo.png" class="float-right" width="150px" alt=""> 
    </nav>
<div class="container-fluid">
  <div class="bg-green">
    <div class="row mt-2 mb-5 p-1">
            <div class="col-12 col-md-6 col-lg-6 ">
                <div class="login">
                    <div class="login-form">
                        <div id="message"></div>
                        <form id="login-form" method="post" role="form">
                          <div class="row">
                                <div class="form-group col-12 col-md-6 offset-md-3">
                                    <input class="form-control" type="text" id="user_name" name="user_name" placeholder="Name" required>
                                </div>
                          </div>
                          <div class="row">
                                <div class="form-group col-12 col-md-6 offset-md-3">
                                    <input class="form-control" type="number" id="user_mobile" name="user_mobile" placeholder="Mobile No." required>
                                </div>
                          </div>
                          <div class="row">
                                <div class="form-group col-12 col-md-6 offset-md-3">
                                    <input class="form-control" type="email" id="user_email" name="user_email" placeholder="Email ID" required>
                                </div>
                          </div>
                          <!-- <div class="row">
                                <div class="form-group col-12 col-md-6 offset-md-3">
                                    <input class="form-control" type="text" id="user_loc" name="user_loc" placeholder="Location" required>
                                </div>
                          </div> -->
                          <!-- <div class="row">
                                <div class="form-group col-12 col-md-6 offset-md-3 consent">
                                    Agree to receive digital communications from Allergan <input type="radio" value="Yes" id="yes" name="consent" checked> Yes <input type="radio" value="No" id="no" name="consent"> No
                                </div>
                          </div> -->
                          
                          <div class="row mt-3">
                            <div class="form-group col-12 col-md-6 offset-md-3">
                                <input type="submit" class="btn btn-submit" value="Login" alt="Submit">
                            </div>
                          </div>  
                    </form>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 text-center mt-5">
                <img src="img/planswithfans.png" class="w-50"alt="">
           </div> 
      </div>
  </div>
    
</div>

<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
$(function(){

	$(document).on('submit', '#login-form', function()
    {  
          $.post('chkforlogin.php', $(this).serialize(), function(data)
          {
             // console.log(data);
              if(data == 's')
              {
                window.location.href='webcast.php';  
              }
              else if (data == '-1')
              {
                  alert('You are already logged in. Please logout and try again.');
                  return false;
              }
              else
              {
                  alert(data);
                  return false;
              }
          });
      
      return false;
    });
});
</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-93480057-15"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-93480057-15');
</script>

</body>
</html>