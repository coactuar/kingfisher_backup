<!doctype html>
<html>
<head>
<meta charset="utf-8">
<link rel="icon" href="img/favicon.png" type="image/png">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title> Live Webcast</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
<link href="/your-path-to-fontawesome/css/fontawesome.css" rel="stylesheet">
  <link href="/your-path-to-fontawesome/css/brands.css" rel="stylesheet">
  <link href="/your-path-to-fontawesome/css/solid.css" rel="stylesheet">
</head>

<body>
<!-- <audio autoplay>
  <source src="" type="audio/mpeg">
</audio> -->
<!-- 
<audio src="audio/ultra.mp3" id="my_audio" loop="loop" autoplay="autoplay"></audio> -->
<!-- 
<audio id="tah_audio" src="audio/ultra.mp3" loop="loop" autoplay></audio> -->
<nav class="navbar m-0 p-0">
        <img src="img/kingfisherlogo.png" width="150px" class="text-center" alt="">
         <img src="img/kp-logo.png" class="float-right" width="150px" alt=""> 
    </nav>

    <div class="container-fluid">
        <div class="row ">

           <div class="col-12 col-md-6 ">
                <form method="post" id="login-form" class=" mt-3">
                <div id="login-message"></div>

                         <!-- <label for="" class="text-white">Login</label> -->
                          <h4 class=" text-white">Login</h4>
                         <div class="form-group">
                         <!-- <input type="text" placeholder="UserName" class="form-control border_change" name="username" Required> -->
				<input type="text" class="form-control" placeholder="Name" name="empName" id="empName" autocomplete="off" required>

                         </div>
                         <div class="form-group">
                         <!-- <input type="number" placeholder="Enter your phonenumber" class="form-control border_change" name="email" Required> -->
                         <input type="email" class="form-control" placeholder="Email ID" name="empid" id="empid" autocomplete="off" required>
                         </div>

                         <div class="form-group">
                         <!-- <input type="number" placeholder="Number" class="form-control border_change"  name="number" Required> -->
                         <input type="number" class="form-control" placeholder="Number" name="number" id="number" autocomplete="off" required>

                         </div>

                         <button id="login" class="login-button "  type="submit">Submit <i class="fas fa-chevron-right"></i></button>
                    
                         
             
        
                </form>
                
            </div> 

           <div class="col-12 col-md-6 text-center mt-5">
                <img src="img/planswithfans.png" class="w-50"alt="">
           </div> 

        </div>
    </div>

<!-- <div class="container-fliud ">
<div class="d-xl-none d-md-none">
<img src="img/mainlogo_hex.png"  class="img-fluid "  width="200px"  alt="">
<img src="img/logo_hex.png"  class="img-fluid float-right " width="120px" alt="">
</div>

<div class="content-area ">
			

				
			
			<div id="loginForm ">
		
			
			<form id="login-form" method="post" role="form">
			<div class="">
			<img src="img/Pahal Logo.png" width=70%  alt=""/>
      
    
			</div>	
			<br/>
			<div class="backs">	
			  <div id="login-message"></div>
			  <div class="input-group">
				<input type="text" class="form-control" placeholder="Name" name="empName" id="empName" autocomplete="off" required>
			  </div>
			  <div class="input-group">
				<input type="email" class="form-control" placeholder="Email ID" name="empid" id="empid" autocomplete="off" required>
			  </div>
			  <div class="input-group">
				<button id="login" class="login-button offset-3"  type="submit">Log in <i class="fas fa-chevron-right"></i></button>
				
			  </div>
			  </div>
			  <div class="offset-4">
			  <p style="font-size:24px" id="demo"></p>
			  </div>
        <div class="d-xl-none d-md-none mt-3">
  <img src="img/bottomlogo.png" class="img-fluid" alt="">
  </div>
			</form> 
		  </div> 

</div> -->
<!-- <audio controls  autoplay>

  <source src="audio/ultra.mp3" type="audio/mpeg">
  Your browser does not support the audio element.
</audio> -->
<!-- <audio id="lobbyMusic" controls  autoplay>
    <source src="audio/ultra.mp3" type="audio/mpeg">
</audio> -->
<!-- <script>
    var lobbyAudio = document.getElementById("lobbyMusic");

    $(function() {
        lobbyAudio.currentTime = 0;
        lobbyAudio.play();
    });
</script> -->

</script>

      
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
// window.onload=function(){
//     document.getElementById("my_audio").play();
//   }

// $(document).ready(function() {
//     $("#tah_audio").get(0).play();
// });
$(document).on('submit', '#login-form', function()
{  



	
  // var phoneNo = document.getElementById('empid');

  // if (phoneNo.value.length < 5 || phoneNo.value.length > 5) {
	//   console.log('Employee ID. is not valid,');
  //   alert("Please Enter The Valid Employee ID Max-Length Should be 5 ");
  //   return false;
  // }


  $.post('chkforlogin.php', $(this).serialize(), function(data)
  {
     // console.log(data);
     $('#login').attr('disabled', false);
      if(data == 's')
      {
        window.location.href='webcast.php';  
      }
      else if (data == '-1')
      {
          $('#login-message').text('You are already logged in. Please logout and try again.');
          $('#login-message').addClass('alert-danger').fadeIn();
      }
      // else if (data == '0')
      // {
      //     $('#login-message').text('Invalid Email Id. Please try again.');
      //     $('#login-message').addClass('alert-danger').fadeIn();
      // }
      else
      {
          $('#login-message').text(data);
          $('#login-message').addClass('alert-danger').fadeIn();
      }
      $('#login').attr('disabled', false);
  });

  
  return false;
});
</script>
</body>
</html>