-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 10, 2022 at 10:40 AM
-- Server version: 5.7.36-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `king11`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pollanswers`
--

CREATE TABLE `tbl_pollanswers` (
  `id` int(11) NOT NULL,
  `poll_id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  `poll_answer` varchar(10) NOT NULL,
  `eventname` varchar(50) NOT NULL,
  `poll_at` datetime NOT NULL,
  `points` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_polls`
--

CREATE TABLE `tbl_polls` (
  `id` int(11) NOT NULL,
  `poll_question` varchar(500) NOT NULL,
  `poll_opt1` varchar(500) NOT NULL,
  `poll_opt2` varchar(500) NOT NULL,
  `poll_opt3` varchar(500) NOT NULL,
  `poll_opt4` varchar(500) NOT NULL,
  `correct_ans` varchar(10) NOT NULL,
  `active` int(11) NOT NULL DEFAULT '0',
  `poll_over` int(11) NOT NULL DEFAULT '0',
  `show_results` int(11) NOT NULL DEFAULT '0',
  `eventname` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_polls`
--

INSERT INTO `tbl_polls` (`id`, `poll_question`, `poll_opt1`, `poll_opt2`, `poll_opt3`, `poll_opt4`, `correct_ans`, `active`, `poll_over`, `show_results`, `eventname`) VALUES
(1, 'Test', '1', '2', '3', '4', 'opt1', 0, 0, 0, 'allergan-beyondiop-03oct'),
(4, 'Other than Punjab, what other states do KXIP represent?', 'Haryana, Himachal', 'Bihar, Jharkhand', 'Nagaland, Manipur', 'Odisha, Uttar Pradesh', 'opt1', 0, 0, 0, 'Kings11'),
(3, 'Who sang KXIPâ€™s original theme song â€˜Dhoom Punjabiâ€™?', 'Mika Singh', 'Diljit Dosanjh', 'Gurdas Maan', 'c.	Daler Mehndi', 'opt2', 0, 0, 0, 'Kings11'),
(5, 'How many players have captained KXIP in the last 12 seasons?', '9', '7', '15', '12', 'opt4', 0, 0, 0, 'Kings11'),
(6, 'Who owns KXIPâ€™s record for most runs in the history of the franchise in the IPL?', 'KL Rahul', 'Shaun Marsh', 'Chris Gayle', 'Yuvraj Singh', 'opt2', 0, 0, 0, 'Kings11'),
(7, 'Moises is the first cricketer born in _____to play for Australia in an international match', 'Spain', 'New Zealand', 'Pakistan', 'Portugal', 'opt4', 0, 0, 0, 'Kings11');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_questions`
--

CREATE TABLE `tbl_questions` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_id` int(10) NOT NULL,
  `user_question` varchar(500) NOT NULL,
  `asked_at` datetime NOT NULL,
  `eventname` varchar(255) NOT NULL,
  `speaker` int(11) NOT NULL DEFAULT '0',
  `answered` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_questions`
--

INSERT INTO `tbl_questions` (`id`, `user_name`, `user_id`, `user_question`, `asked_at`, `eventname`, `speaker`, `answered`) VALUES
(1, 'naga', 1, 'hello', '2021-09-16 15:47:03', 'allergan-beyondiop-03oct', 0, 0),
(2, 'naga', 4, 'hello', '2021-09-16 16:05:56', 'Kings11', 0, 0),
(3, 'Shiva Shakti', 10, 'your most memorable match in IPL?', '2021-09-16 16:49:06', 'Kings11', 0, 0),
(4, 'Shiva Shakti', 10, 'How does the interaction work?', '2021-09-16 16:55:13', 'Kings11', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_phone` varchar(20) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `joining_date` datetime NOT NULL,
  `login_date` datetime DEFAULT NULL,
  `logout_date` datetime DEFAULT NULL,
  `logout_status` int(11) NOT NULL DEFAULT '0' COMMENT '1=loggedin',
  `eventname` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `user_name`, `user_phone`, `user_email`, `joining_date`, `login_date`, `logout_date`, `logout_status`, `eventname`) VALUES
(1, 'naga', '9700467764', 'neeraj@coact.co.in', '2021-09-17 15:03:23', '2021-09-30 17:38:29', '2021-09-30 17:38:32', 0, 'Kings11'),
(2, 'naga', '9700467764', 'content@hammerheadexperiences.com', '2021-09-17 15:15:30', '2021-09-30 17:38:29', '2021-09-30 17:38:59', 1, 'Kings11'),
(3, 'naga', '9700467764', 'akshatjharia@gmail.com', '2021-09-17 15:16:10', '2021-09-30 17:38:29', '2021-09-30 17:38:59', 1, 'Kings11'),
(4, 'naga', '9700467764', 'shakti.alchemist@gmail.com', '2021-09-17 15:17:33', '2021-09-30 17:38:29', '2021-09-30 17:38:59', 1, 'Kings11'),
(5, 'naga', '9700467764', 'steevans@ubmail.com', '2021-09-17 15:20:30', '2021-09-30 17:38:29', '2021-09-30 17:38:59', 1, 'Kings11'),
(6, 'likitha', '8105278762', 'likitha@coact.co.in', '2021-12-27 15:39:44', '2021-12-27 15:39:44', '2021-12-27 16:27:46', 1, 'Kings11'),
(7, 'priyanaka', '09876543', 'prriyay@coact.co.in', '2022-01-10 15:01:03', '2022-01-10 15:01:03', '2022-01-10 16:04:27', 0, 'Kings11');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_pollanswers`
--
ALTER TABLE `tbl_pollanswers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_polls`
--
ALTER TABLE `tbl_polls`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_pollanswers`
--
ALTER TABLE `tbl_pollanswers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_polls`
--
ALTER TABLE `tbl_polls`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
